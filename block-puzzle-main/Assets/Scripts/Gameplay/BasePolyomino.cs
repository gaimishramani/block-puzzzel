﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

namespace GamePlay
{
    public class BasePolyomino : MonoBehaviour
    {
        #region PUBLIC_VARS
        public BoxCollider2D mainCollider;
        public bool isOnDrag;
        public bool isployominoPlaceable;
        #endregion

        #region PRIVATE_VARS
        protected Vector3 startPos;
        #endregion

        #region UNITY_CALLBACKS
        private void Start()
        {
            startPos = transform.position;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public virtual void Init()
        {

        }
        public virtual void CheckCollideRule()
        {

        }
        protected Cell GetCollideCell(Transform rayPoint)
        {
            RaycastHit2D raycastHit = Physics2D.Raycast(rayPoint.position, Vector3.forward, 1);
            if (raycastHit.collider != null)
            {
                return raycastHit.transform.GetComponentInParent<Cell>();
            }
            return null;
        }
        public void SetMainCollider(bool state)
        {
            //  mainCollider.gameObject.SetActive(state);
        }

        public virtual void OnDrag()
        {
            isOnDrag = true;
            SetMainCollider(false);
            // transform.localScale = Vector3.one;
        }

        public virtual void OnDrop(bool isPlaced)
        {
            isOnDrag = false;
        }
        public virtual void CheckPlaceAvailable()
        {

        }
        public void StartAnimation(Transform startTransform, CustomTransform endEndTransform, Action action = null)
        {
            StartCoroutine(LearpObject(startTransform, endEndTransform, action));
        }
        public void WaitAndDestroy()
        {
            StartCoroutine(WaitAndDestroyCO());
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES

        public IEnumerator LearpObject(Transform from, CustomTransform to, Action action)
        {
            Vector3 startPos = from.transform.position;
            Vector3 endPos = to.position;
            Vector3 startScale = from.transform.localScale;
            Vector3 endScale = to.scale;
            float i = 0;
            float speed = 1 / 0.1f;
            while (i < 1)
            {
                i += Time.deltaTime * speed;
                from.transform.position = Vector3.Lerp(startPos, endPos, i);
                from.transform.localScale = Vector3.Lerp(startScale, endScale, i);
                yield return 0;
            }
            action?.Invoke();
        }
        private IEnumerator WaitAndDestroyCO()
        {
            yield return null;
            yield return null;
            Destroy(gameObject);
        }
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}