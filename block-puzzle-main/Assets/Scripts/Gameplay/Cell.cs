﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class Cell : MonoBehaviour
    {
        #region PUBLIC_VARS
        public int rowIndex;
        public int colIndex;
        public Block block;
        public GameObject cellBorderPS;
        public ParticleSystem cellEffectPS;
        public List<ParticleSystem> destoryParticleSystems;
        public List<ParticleSystem> hintParticleSystems;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public bool IsEmpty()
        {
            return block.spriteRenderer.enabled == false;
        }
        public void SetTemporarySprite(SpriteTypes colorType, float alphaValue = 1, bool withAnimation = true)
        {
            block.temporarySpriteRenderer.sprite = GameManager.Instance.colorData.GetSprite(colorType);
            Color color = block.temporarySpriteRenderer.color;
            color.a = alphaValue;
            block.temporarySpriteRenderer.color = color;
            block.temporarySpriteRenderer.enabled = true;
            if (alphaValue == 1 && withAnimation)
                cellBorderPS.SetActive(true);
        }
        public void ResetTemporarySprite()
        {
            block.temporarySpriteRenderer.enabled = false;
            cellBorderPS.SetActive(false);
            cellEffectPS.Stop();
        }
        public void DestroyBlock()
        {
            if (!IsEmpty())
                PlayDestroyAnimation();
            ResetData();
        }   
        public void DestroyBlock(SpriteTypes colorType)
        {
            block.colorType = colorType;
            if (!IsEmpty())
                PlayDestroyAnimation();
            ResetData();
        }

        public void ResetData()
        {
            ResetTemporarySprite();
            ResetBlockSprite();
        }

        public void SetBlock(SpriteTypes colorType)
        {
            block.colorType = colorType;
            block.spriteRenderer.sprite = GameManager.Instance.colorData.GetSprite(colorType);
            block.spriteRenderer.enabled = true;
        }
        public void ChangeCellEffectLayer(int number)
        {
            cellEffectPS.GetComponent<ParticleSystemRenderer>().sortingOrder = number;
        }
        public void ActivateCellEffect()
        {
            cellEffectPS.Play();
        }
        public void ActivateCellBorderEffect()
        {
            cellBorderPS.SetActive(true);
        }
        public void DeactivateCellEffect()
        {
            cellEffectPS.Stop();
        }
        public void PlayHintFX()
        {
            hintParticleSystems.ForEach(x => x.Play());
        }
        public void StopHintFX()
        {
            hintParticleSystems.ForEach(x => x.Stop());
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void ResetBlockSprite()
        {
            block.colorType = SpriteTypes.NONE;
            block.spriteRenderer.enabled = false;
        }
        private void PlayDestroyAnimation()
        {
            destoryParticleSystems[0].GetComponent<ParticleSystemRenderer>().material = GameManager.Instance.materialDataSO.GetMaterial(block.colorType);
            destoryParticleSystems.ForEach(x => x.Play());
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}