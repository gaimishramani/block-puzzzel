﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class PolyominoSlot : MonoBehaviour
    {
        #region PUBLIC_VARS
        public BasePolyomino polyomino;
        public BoxCollider2D boxCollider;
        #endregion

        #region PRIVATE_VARS
        private BasePolyomino lastPolyomino;
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public void SetPolyomino(BasePolyomino polyomino)
        {
            this.polyomino = polyomino;
            this.polyomino.transform.localScale = Vector3.one / 2;
            polyomino.Init();
        }
        public void SetBoosterPolyomino()
        {

        }
        public BasePolyomino GetPolyomino()
        {
            return polyomino;
        }
        public bool IsEmpty()
        {
            return polyomino == null;
        }
        public void ResetPolyomino()
        {
            if (polyomino != null)
            {
                polyomino.WaitAndDestroy();
                polyomino = null;
            }
        }
        public void CheckPlaceAvailable()
        {
            if (polyomino == null)
                return;
            polyomino.CheckPlaceAvailable();
        }
        public void SaveLastPolyomino()
        {
            if (polyomino == null)
                return;
            lastPolyomino = polyomino;
            polyomino.gameObject.SetActive(false);
            polyomino = null;
        }
        public void SetLastPolyomino()
        {
            if (polyomino != null)
                ResetPolyomino();
            if (lastPolyomino == null)
                return;
            polyomino = lastPolyomino;
            polyomino.gameObject.SetActive(true);
            lastPolyomino = null;
        }
        public void ResetLastPolyomino()
        {
            lastPolyomino = null;
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}