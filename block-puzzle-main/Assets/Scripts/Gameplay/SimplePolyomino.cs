﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GamePlay
{
    public class SimplePolyomino : BasePolyomino
    {
        #region PUBLIC_VARS
        public SpriteTypes colorType;
        public List<PolyominoBlock> polyominoBlocks;
        public Cell dropCell;
        public Transform centerBlockTransform;
        public Cell placeableDropCell;
        #endregion

        #region PRIVATE_VARS
        private Cell lastDropCell;
        public List<PolyominoDistributeData> rowWiseDistributeDatas = new List<PolyominoDistributeData>();
        public List<PolyominoDistributeData> colWiseDistributeDatas = new List<PolyominoDistributeData>();

        private Coroutine WaitAndHintCO;
        #endregion

        #region UNITY_CALLBACKS
        private void Start()
        {
            transform.localPosition /= 2;
            startPos = transform.position;
        }
        private void Update()
        {
            if (isOnDrag)
                CheckCollideRule();
        }

        #endregion

        #region PUBLIC_FUNCTIONS
        public override void Init()
        {
            base.Init();
            colorType = GameManager.Instance.colorData.GetRandomColorType();
            SetSprite(colorType);
        }

        public override void OnDrag()
        {
            base.OnDrag();
            SetBlockSize(0.9f);
            WaitAndHintCO = StartCoroutine(WaitAndHint(5));
        }
        public override void OnDrop(bool isPlaced)
        {
            base.OnDrop(isPlaced);
            SoundManager.Instance.PlayDropPolyomino();
            GameManager.Instance.grid.StopHintPS(this);
            if (WaitAndHintCO != null)
                StopCoroutine(WaitAndHintCO);
            if (isPlaced)
            {
                GameManager.Instance.currentGamePlay.UpdateScore(polyominoBlocks.Count);
                StartAnimation(transform, new CustomTransform(dropCell.transform.position, Vector3.one), delegate
                  {
                      PolyominoHolder.Instance.polyominoSlots.Find(x => x.polyomino == this).ResetPolyomino();
                      GameManager.Instance.grid.SetPolyominoInCell(this);
                  });

            }
            else
            {
                StartAnimation(transform, new CustomTransform(startPos, Vector3.one / 2), delegate { SetBlockSize(1); });
                SetMainCollider(true);
                InputManager.Instance.inputState = true;
            }
        }
        public void SetSprite(SpriteTypes colorType)
        {
            SetIfPlaceable(colorType);
            for (int i = 0; i < polyominoBlocks.Count; i++)
            {
                polyominoBlocks[i].spriteRenderer.sprite = GameManager.Instance.colorData.GetSprite(colorType);
            }
        }
        public void SetUnplacedSprite()
        {
            SetSprite(SpriteTypes.UN_PLACED);
        }

        public void SetBlockSize(float size)
        {
            polyominoBlocks.ForEach(x => x.transform.localScale = new Vector3(size, size, size));
        }
        public override void CheckPlaceAvailable()
        {
            base.CheckPlaceAvailable();

            Grid grid = GameManager.Instance.grid;
            for (int i = 0; i < grid.cells.Count; i++)
            {
                dropCell = grid.cells[i];
                if (RuleManager.Instance.IsPlaceAvailable(this))
                {
                    SetSprite(colorType);
                    placeableDropCell = grid.cells[i];
                    dropCell = null;
                    return;
                }
            }
            SetUnplacedSprite();
            dropCell = null;
            placeableDropCell = null;
        }
        public override void CheckCollideRule()
        {
            lastDropCell = dropCell;
            dropCell = GetCollideCell(centerBlockTransform);
            if (dropCell == null)
            {
                RuleManager.Instance.CheckExitRule(lastDropCell);
            }
            else
            {
                if (lastDropCell != dropCell)
                {
                    RuleManager.Instance.CheckExitRule(lastDropCell);
                    RuleManager.Instance.CheckEntryRule(this);
                }
            }
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        private void SetIfPlaceable(SpriteTypes colorType)
        {
            if (SpriteTypes.UN_PLACED == colorType)
            {
                isployominoPlaceable = false;
                SetMainCollider(false);
            }
            else
            {
                isployominoPlaceable = true;
                SetMainCollider(true);
            }
        }
        #endregion



        #region CO-ROUTINES
        public IEnumerator WaitAndHint(float time)
        {
            yield return new WaitForSeconds(time);
            GameManager.Instance.grid.PlayHintPS(this);
        }

        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion

        #region SET_UP
        [ContextMenu("SetUp")]
        private void SetUp()
        {
            SetRowWiseData();
            SetColWiseData();
        }
        [ContextMenu("Set Block Pos")]
        private void PolyominoBlockPosSetUp()
        {
            foreach (var item in polyominoBlocks)
            {
                item.transform.localPosition = new Vector3(item.colIndex, item.rowIndex, 0);
            }
        }
        private void SetRowWiseData()
        {
            rowWiseDistributeDatas.Clear();
            Debug.Log("row");
            for (int i = 0; i < polyominoBlocks.Count; i++)
            {
                if (DoesContainInRowList(polyominoBlocks[i].rowIndex))
                {
                    rowWiseDistributeDatas.Find(x => x.index == polyominoBlocks[i].rowIndex).polyominoBlocks.Add(polyominoBlocks[i]);
                }
                else
                {
                    PolyominoDistributeData polyominoDistributeData = new PolyominoDistributeData();
                    polyominoDistributeData.polyominoBlocks.Add(polyominoBlocks[i]);
                    polyominoDistributeData.index = polyominoBlocks[i].rowIndex;
                    rowWiseDistributeDatas.Add(polyominoDistributeData);
                }
            }
        }

        private void SetColWiseData()
        {
            colWiseDistributeDatas.Clear();
            Debug.Log("col");
            for (int i = 0; i < polyominoBlocks.Count; i++)
            {
                if (DoesContainInColList(polyominoBlocks[i].colIndex))
                {
                    colWiseDistributeDatas.Find(x => x.index == polyominoBlocks[i].colIndex).polyominoBlocks.Add(polyominoBlocks[i]);
                }
                else
                {
                    PolyominoDistributeData polyominoDistributeData = new PolyominoDistributeData();
                    polyominoDistributeData.polyominoBlocks.Add(polyominoBlocks[i]);
                    polyominoDistributeData.index = polyominoBlocks[i].colIndex;
                    colWiseDistributeDatas.Add(polyominoDistributeData);
                }
            }
        }


        private bool DoesContainInRowList(int index)
        {
            return rowWiseDistributeDatas.Find(x => x.index == index) != null;
        }
        private bool DoesContainInColList(int index)
        {
            return colWiseDistributeDatas.Find(x => x.index == index) != null;
        }
        #endregion
    }

    [Serializable]
    public class PolyominoDistributeData
    {
        public List<PolyominoBlock> polyominoBlocks = new List<PolyominoBlock>();
        public int index;
    }

}