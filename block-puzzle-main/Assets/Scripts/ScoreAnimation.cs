﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScoreAnimation : MonoBehaviour
{
    #region PUBLIC_VARS
    public Animator animator;
    public TextMeshProUGUI scoreText;
    public SpriteRenderer spriteRenderer;
    #endregion

    #region PRIVATE_VARS
    #endregion

    #region UNITY_CALLBACKS

    #endregion

    #region PUBLIC_FUNCTIONS
    public void PlayAnimation()
    {
        gameObject.SetActive(true);
        animator.Play("ScoreAnim");
        StartCoroutine(WaitAndStopAnimation(1.16f));
    }
    public void SetData(int score ,Sprite sprite)
    {
        scoreText.text = score.ToString();
        spriteRenderer.sprite = sprite;
    }
    #endregion

    #region PRIVATE_FUNCTIONS
    #endregion

    #region CO-ROUTINES
    private IEnumerator WaitAndStopAnimation(float time)
    {
        yield return new WaitForSeconds(time);
        gameObject.SetActive(false);
    }
    #endregion

    #region EVENT_HANDLERS
    #endregion

    #region UI_CALLBACKS
    #endregion
}
