﻿namespace GamePlay
{
    public enum RewardType
    {
        ONE_BLOCK_BOOSTER,
        BIG_BOMB_BOOSTER,
        RAINBOW_BOOSTER,
        TRY_NEXT_TIME
    }
}