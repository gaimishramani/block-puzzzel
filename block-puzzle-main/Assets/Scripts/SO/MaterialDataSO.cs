﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    [CreateAssetMenu(fileName = "MaterialDataSO", menuName = "ScriptableObject/MaterialDataSO", order = 1)]
    public class MaterialDataSO : ScriptableObject
    {
        #region PUBLIC_VARS
        public List<MaterialData> materialDatas;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        public Material GetMaterial(SpriteTypes colorType)
        {
            return materialDatas.Find(x => x.colorType == colorType).material;
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
    [System.Serializable]
    public class MaterialData
    {
        public Material material;
        public SpriteTypes colorType;
    }
}