﻿using DG.Tweening;
using GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EsterHomePlayView : MonoBehaviour
{
    public void OnPlayClick()
    {
        GameManager.Instance.currentGamePlay = UiManager.Instance.GetGamePlayViewByType(GamePlayType.Ester);
        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
        DataManager.Instance.ResetPolyominoGeneratorCount();
        DOVirtual.DelayedCall(0.5f, () => { });
        gameObject.SetActive(false);
        GameManager.Instance.Init();
        UiManager.Instance.GetGamePlayViewByType(GamePlayType.Helloween).gameObject.SetActive(true);
        SoundManager.Instance.PlayButtonClickSFX();

    }
    public void OnTimerBasedPlayModeClick()
    {
        GameManager.Instance.currentGamePlay = UiManager.Instance.GetTimerBasedGamePlayViewByType(GamePlayType.Ester);
        PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
        DataManager.Instance.ResetPolyominoGeneratorCount();
        DOVirtual.DelayedCall(0.5f, () => { });
        gameObject.SetActive(false);
        GameManager.Instance.Init();
        UiManager.Instance.GetTimerBasedGamePlayViewByType(GamePlayType.Helloween).gameObject.SetActive(true);
        SoundManager.Instance.PlayButtonClickSFX();
    }
}
