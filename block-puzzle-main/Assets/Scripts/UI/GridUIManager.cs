﻿using GamePlay;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridUIManager : MonoBehaviour
{
    public SpriteRenderer[] cellsSprites;
    public CellSpriteData[] cellSpriteDatas;


    public void SetCellSpriteById(GamePlayType gamePlayType)
    {
        Sprite sprite = GetSpriteByID(gamePlayType);
        for (int i = 0; i < cellsSprites.Length; i++)
        {
            cellsSprites[i].sprite = sprite;
        }
    }

    private Sprite GetSpriteByID(GamePlayType gamePlayType)
    {
        for (int i = 0; i < cellSpriteDatas.Length; i++)
        {
            if (gamePlayType == cellSpriteDatas[i].gamePlayType)
            {
                return cellSpriteDatas[i].sprite;
            }
        }
        return null;
    }

    [Serializable]
    public class CellSpriteData
    {
        public GamePlayType gamePlayType;
        public Sprite sprite;
    }
}
