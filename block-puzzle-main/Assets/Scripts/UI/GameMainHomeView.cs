﻿using GamePlay;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMainHomeView : MonoBehaviour
{
    public void ShowView()
    {
        gameObject.SetActive(true);
    }

    public void HideView()
    {
        gameObject.SetActive(false);
    }
    public void OnPlayButtonClick()
    {
        HideView();
        UiManager.Instance.themeSelectionView.gameObject.SetActive(true);
    }
}
