﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GamePlay
{
    public class GameOverView : MonoBehaviour
    {
        #region PUBLIC_VARS
        public Text currentScoreText;
        public Text highScoreText;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void OnEnable()
        {
            DisplayScores();
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        #endregion

        #region PRIVATE_FUNCTIONS
        private void DisplayScores()
        {
            currentScoreText.text = GameManager.Instance.currentGamePlay.currentScore.ToString();
            highScoreText.text = GameManager.Instance.currentGamePlay.highScore.ToString();
            UiManager.Instance.playfabManager.SendLeaderboard(GameManager.Instance.currentGamePlay.highScore);
            DataAnalyster.Instance.SetDataOnGameComplite(GameManager.Instance.currentGamePlay.currentScore);
        }
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnHomeClick()
        {
            gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
            GameManager.Instance.grid.ResetAllCells();
            PolyominoHolder.Instance.ClearAllPolyomino();
            GameManager.Instance.isGameOver = false;
            GameManager.Instance.grid.gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.ResetScore();
            PlayerPrefs.DeleteKey(Constants.CURRENT_SCORE);
            SoundManager.Instance.PlayButtonClickSFX();
            UiManager.Instance.gameMainHomeView.ShowView();
        }
        public void OnRestart()
        {
            PlayerPrefs.SetInt(Constants.CURRENT_SCORE, 0);
            DataManager.Instance.ResetPolyominoGeneratorCount();
            gameObject.SetActive(false);
            SoundManager.Instance.PlayButtonClickSFX();
            GameManager.Instance.RestartGame();
        }

        public void OnLeaderBoardClick()
        {
            gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.gameObject.SetActive(false);
            UiManager.Instance.leaderBoardView.gameObject.SetActive(true);
            GameManager.Instance.grid.ResetAllCells();
            PolyominoHolder.Instance.ClearAllPolyomino();
            GameManager.Instance.isGameOver = false;
            GameManager.Instance.grid.gameObject.SetActive(false);
            GameManager.Instance.currentGamePlay.ResetScore();
            PlayerPrefs.DeleteKey(Constants.CURRENT_SCORE);
            SoundManager.Instance.PlayButtonClickSFX();
        }
        #endregion
    }
}