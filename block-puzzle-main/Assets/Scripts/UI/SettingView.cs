﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class SettingView : MonoBehaviour
    {
        #region PUBLIC_VARS
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        #endregion

        #region PUBLIC_FUNCTIONS
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        public void OnCloseClick()
        {
            gameObject.SetActive(false);
            SoundManager.Instance.PlayButtonClickSFX();
        }
        public void OnSoundClick()
        {

        }
        public void RateUsClick()
        {

        }
        public void OnShareClick()
        {

        }
        #endregion
    }
}