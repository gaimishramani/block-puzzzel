﻿using UnityEngine;
using UnityEngine.UI;

namespace GamePlay
{
    public class ThemeSelectionView : MonoBehaviour
    {
        [SerializeField] private Sprite haloweenBGSprite;
        [SerializeField] private Sprite christmasBGSprite;
        [SerializeField] private Sprite esterBGSprite;

        [SerializeField] private Image bGImage;

        public void ShowView()
        {
            gameObject.SetActive(true);
        }

        public void HideView()
        {
            gameObject.SetActive(false);
        }

        public void OnHalloweenPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;
            uiManager.halloweenHomeView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Helloween);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Helloween);
            bGImage.sprite = haloweenBGSprite;
        }

        public void OnChristmasPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;

            uiManager.christmasHomeView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Christmas);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Christmas);
            bGImage.sprite = christmasBGSprite;

        }

        public void OnEsterPlayClick()
        {
            HideView();
            UiManager uiManager = UiManager.Instance;

            uiManager.esterHomePlayView.gameObject.SetActive(true);
            GameManager.Instance.SetColorDataByGamePlay(GamePlayType.Ester);
            uiManager.gridUIManager.SetCellSpriteById(GamePlayType.Ester);
            bGImage.sprite = esterBGSprite;

        }
    }

}