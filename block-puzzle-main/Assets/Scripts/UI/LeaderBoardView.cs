﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace GamePlay
{
    public class LeaderBoardView : MonoBehaviour
    {
        #region PUBLIC_VARS

        public TMP_Text internetMessage;

        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS

        private void OnEnable()
        {
            UiManager.Instance.playfabManager.SetData();
        }

        private void Update()
        {
            //if(Input.GetKey(KeyCode.Escape))
            //{
            //    gameObject.SetActive(false);
            //    //UiManager.Instance.homeView.gameObject.SetActive(true);
            //}
        }

        #endregion

        #region PUBLIC_FUNCTIONS

        public void SetInternetMessage(string msg)
        {
            internetMessage.text = msg;
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS

        public void OnCloseClick()
        {
            //Debug.Log("OnCloseClick");
            //SoundManager.Instance.PlayButtonClickSFX();
            //UiManager.Instance.homeView.gameObject.SetActive(true);
            //gameObject.SetActive(false);
        }

        #endregion
    }
}