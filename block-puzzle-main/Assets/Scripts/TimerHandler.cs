﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimerHandler : MonoBehaviour
{
    public int timeInSec;
    
    public int GetMin(int sec)
    {
        return TimeSpan.FromSeconds(sec).Minutes;
    }
    public int GetSec(int sec)
    {
        return TimeSpan.FromSeconds(sec).Seconds;
    }

}
