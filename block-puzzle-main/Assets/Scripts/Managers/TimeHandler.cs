﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Globalization;
namespace GamePlay
{
    public class TimeHandler
    {
        #region PUBLIC_VARIABLES

        #endregion
        #region PRIVATE_VARIABLES

        #endregion
        #region UNITY_CALLBACKS

        #endregion
        #region PUBLIC_FUNCTIONS
        public static void SetLastCollectTime()
        {
            DataManager.Instance.LastTimeToSpin = SerializeDate(DateTime.Now);
        }
        public static TimeSpan GetTimeDifference(string storedTimerString)
        {
            DateTime lastTime = DeserializeDate(storedTimerString);

            TimeSpan difference = DateTime.Now - lastTime;

            return difference;
        }

        public static TimeSpan GetRemainingTime(DateTime time)
        {
            TimeSpan difference = time - DateTime.Now;
            return difference.TotalSeconds > 0 ? difference : TimeSpan.Zero;
        }

        public static float GetRemainingSeconds(float sourceTime, string storedTimeString)
        {
            float remainingTime = sourceTime - (float)GetTimeDifference(storedTimeString).Duration().TotalSeconds;
            return remainingTime;
        }
        public static string SerializeDate(DateTime dateTime)
        {
            return dateTime.Ticks.ToString();
        }
        public static DateTime DeserializeDate(string ticks)
        {
            long longTicks = long.Parse(ticks);
            return new DateTime(longTicks, DateTimeKind.Local);
        }
        #endregion
        #region PRIVATE_FUNCTIONS

        #endregion
    }
}