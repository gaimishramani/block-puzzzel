﻿using UnityEngine;

[CreateAssetMenu(fileName = "TimerDataSO", menuName = "ScriptableObject/TimerDataSO")]

public class TimerDataSO : ScriptableObject
{
    [Header("Time in Sec")]
    public int time;
}
