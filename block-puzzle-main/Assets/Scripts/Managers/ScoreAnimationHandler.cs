﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{

    public class ScoreAnimationHandler : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static ScoreAnimationHandler Instance;
        public ScoreAnimation scoreAnimation;
        public Sprite good;
        public Sprite cool;
        public Sprite amazingg;
        #endregion

        #region PRIVATE_VARS
        private Transform cellTransform;
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void SetCellTransform(Transform cellTransform)
        {
            this.cellTransform = cellTransform; 
        }
        public void ActivateEffects(int score)
        {
            if(score==30)
            {
                scoreAnimation.SetData(score, good);
                SoundManager.Instance.PlayGoodSound();
            }
            else if(score == 60)
            {
                scoreAnimation.SetData(score, cool);
                SoundManager.Instance.PlayCoolSound();
            }
            else
            {
                scoreAnimation.SetData(score, amazingg);
                SoundManager.Instance.PlayAmazingSound();
            }
            scoreAnimation.transform.position = cellTransform.position;
            scoreAnimation.PlayAnimation();
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}