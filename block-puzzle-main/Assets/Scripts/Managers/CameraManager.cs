﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GamePlay
{
    public class CameraManager : MonoBehaviour
    {
        #region PUBLIC_VARIABLES
        //public float worldWidth;
        public static CameraManager Instance;
        public new Camera camera;

        [Header("Bounds Settings")]
        public SpriteRenderer requiredGameplayBounds;
        public SpriteRenderer maximumGameplayBounds;
        #endregion

        #region PRIVATE_VARIABLES
        //private float reqAspect = 2160f / 1080f;
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            if (Instance == null)
                Instance = this;
        }
        //private void Awake()
        //{
        //    SetWorld();
        //    //SetUp();
        //}
        private void Start()
        {
            OnInitWorldWithForCamera();
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void OnInitWorldWithForCamera()
        {
            SetWorld();
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        /*private void SetUp()
        {
            float x = worldWidth;
            float y;
            Camera myCam = this.gameObject.GetComponent<Camera>();

            if (myCam != null)
            {
                if (myCam.aspect < reqAspect)
                {
                    y = x / myCam.aspect;
                    myCam.orthographicSize = y;
                }
            }
        }*/

        private void SetWorld()
        {
            Camera myCam = camera;

            double currentAspectRatio = Math.Round((float)Screen.width / (float)Screen.height, 5);
            // Debug.Log("Current : " + currentAspectRatio);
            double requiredGameplayAspectRatio = Math.Round(requiredGameplayBounds.transform.localScale.x / requiredGameplayBounds.transform.localScale.y, 5);
            // Debug.Log("Required : " + requiredGameplayAspectRatio);
            double maxGameplayAspectRatio = Math.Round(maximumGameplayBounds.transform.localScale.x / maximumGameplayBounds.transform.localScale.y, 5);
            // Debug.Log("Max : " + maxGameplayAspectRatio);

            myCam.transform.position = requiredGameplayBounds.transform.position;
            myCam.orthographicSize = requiredGameplayBounds.transform.localScale.y / 2;

            Vector2 currentWidthLimits = new Vector2(myCam.ScreenToWorldPoint(new Vector3(0, 0, 0)).x, myCam.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0)).x);
            // Debug.Log(string.Format("Current Limits : {0} , {1}", currentWidthLimits.x, currentWidthLimits.y));
            Vector2 maxWidthLimits = new Vector2(maximumGameplayBounds.transform.position.x - maximumGameplayBounds.transform.localScale.x / 2, maximumGameplayBounds.transform.position.x + maximumGameplayBounds.transform.localScale.x / 2);
            // Debug.Log(string.Format("Max Limits : {0} , {1}", maxWidthLimits.x, maxWidthLimits.y));
            Vector2 requiredGameplayWidthLimits = new Vector2(requiredGameplayBounds.transform.position.x - requiredGameplayBounds.transform.localScale.x / 2, requiredGameplayBounds.transform.position.x + requiredGameplayBounds.transform.localScale.x / 2);
            // Debug.Log(string.Format("Max Limits : {0} , {1}", requiredGameplayWidthLimits.x, requiredGameplayWidthLimits.y));
            float camWidthInWorldScale = myCam.transform.InverseTransformPoint(myCam.ScreenToWorldPoint(new Vector3(Screen.width, 0, 0))).x * 2;
            // Debug.Log("Width Screen : " + camWidthInWorldScale);
            float camHeightInWorldScale = myCam.transform.InverseTransformPoint(myCam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0))).y * 2;
            //  Debug.Log("Height Screen : " + camHeightInWorldScale);

            //Check For Horizontal Inbound With Required Gameplay
            if (camWidthInWorldScale < (requiredGameplayWidthLimits.y - requiredGameplayWidthLimits.x))
            {
                //Debug.Log("In Bound Width with Gameplay Area");
                // Set Width According to Required Gameplay Increasing Height
                myCam.orthographicSize = ((requiredGameplayWidthLimits.y - requiredGameplayWidthLimits.x) / myCam.aspect) / 2;
                //
                Vector2 currentHeightLimits = new Vector2(myCam.ScreenToWorldPoint(new Vector3(0, 0, 0)).y, myCam.ScreenToWorldPoint(new Vector3(0, Screen.height, 0)).y);
                Vector2 maxHeightLimits = new Vector2(maximumGameplayBounds.transform.position.y - maximumGameplayBounds.transform.localScale.y / 2, maximumGameplayBounds.transform.position.y + maximumGameplayBounds.transform.localScale.y / 2);
                if (currentHeightLimits.x < maxHeightLimits.x || currentHeightLimits.y > maxHeightLimits.y)
                {
                    //Debug.Log("Out Of Bound Height With Max Area");
                    if (camHeightInWorldScale < (maxHeightLimits.y - maxHeightLimits.x))
                    {
                        // Debug.Log("Setting Camera Position...");
                        Vector3 newPos = myCam.transform.position;
                        if (currentHeightLimits.x < maxHeightLimits.x)
                        {
                            newPos.y += (maxHeightLimits.x - currentHeightLimits.x);
                        }
                        else if (currentHeightLimits.y > maxHeightLimits.y)
                        {
                            newPos.y += (maxHeightLimits.y - currentHeightLimits.y);
                        }
                        myCam.transform.position = newPos;
                    }
                    else
                    {
                        Debug.LogError("Camera Position Cannot be Set to Cover Max Area In Height.. Please Increase Max Area.");
                    }
                }
            }
            else if (currentWidthLimits.x < maxWidthLimits.x || currentWidthLimits.y > maxWidthLimits.y)
            {
                // Debug.Log("Out Of Bound Width With Max Area");
                if (camWidthInWorldScale < (maxWidthLimits.y - maxWidthLimits.x))
                {
                    //Debug.Log("Setting Camera Position...");
                    Vector3 newPos = myCam.transform.position;
                    if (currentWidthLimits.x < maxWidthLimits.x)
                    {
                        newPos.x += (maxWidthLimits.x - currentWidthLimits.x);
                    }
                    else if (currentWidthLimits.y > maxWidthLimits.y)
                    {
                        newPos.x += (maxWidthLimits.y - currentWidthLimits.y);
                    }
                    myCam.transform.position = newPos;
                }
                else
                {
                    Debug.LogError("Camera Position Cannot be Set to Cover Max Area In Width.. Please Increase Max Area.");
                }
            }
        }
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region COROUTINES
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}