﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace GamePlay
{
    public class RuleManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static RuleManager Instance;
        public List<Cell> destroyCells;
        public List<DestroyCellData> destroyCellDatas = new List<DestroyCellData>();
        public List<Cell> cells;
        #endregion

        #region PRIVATE_VARS

        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region DESTROY_RULE

        public void CheckDropRule(SimplePolyomino polyomino)
        {
            Drop(polyomino);
        }
        private void Drop(SimplePolyomino polyomino)
        {
            Grid grid = GameManager.Instance.grid;
            ScoreAnimationHandler.Instance.SetCellTransform(polyomino.dropCell.transform);
            destroyCells.Clear();
            List<DestroyCellData> tempDestroyCellDatas;
            foreach (PolyominoBlock polyominoBlock in polyomino.polyominoBlocks)
            {
                tempDestroyCellDatas = CheckDestroyRule(polyominoBlock, polyomino);
                if (tempDestroyCellDatas.Count != 0)
                {
                    foreach (var item in tempDestroyCellDatas)
                    {
                        destroyCells.AddRange(item.cells);
                    }
                    destroyCellDatas.AddRange(tempDestroyCellDatas);
                    tempDestroyCellDatas = null;
                }
            }
            DestroyCells(polyomino.colorType);
        }

        private void DestroyCells(SpriteTypes colorType)
        {
            destroyCells = destroyCells.Distinct().ToList();
            if (destroyCellDatas == null || destroyCellDatas.Count == 0)
            {
                GameManager.Instance.grid.PerformActionAfterDestroyBlock();
                return;
            }
            int numOfDestroyLine = Mathf.CeilToInt((float)destroyCells.Count / (float)Constants.RAW_SIZE);

            int score = numOfDestroyLine * (numOfDestroyLine + 1) / 2 * 10;
            GameManager.Instance.currentGamePlay.UpdateScore(score);
            SoundManager.Instance.PlayDestorySFX();
            GameManager.Instance.grid.DestroyBlocks(destroyCellDatas, colorType);
            if (score >= 30)
                ScoreAnimationHandler.Instance.ActivateEffects(score);
            destroyCellDatas.Clear();
        }
        private List<DestroyCellData> CheckDestroyRule(PolyominoBlock polyominoBlock, SimplePolyomino polyomino)
        {
            int rowIndex = polyomino.dropCell.rowIndex + polyominoBlock.rowIndex, colIndex = polyomino.dropCell.colIndex + polyominoBlock.colIndex;
            Grid grid = GameManager.Instance.grid;
            List<DestroyCellData> destroyCellDatas = new List<DestroyCellData>();
            DestroyCellData destroyCellData = new DestroyCellData();
            if (grid.IsItRowFull(rowIndex))
            {
                for (int i = colIndex; i >= 0; i--)
                {
                    destroyCellData.Add(grid.cells2dArray[rowIndex, i]);
                }
                if (destroyCellData != null)
                {
                    destroyCellDatas.Add(destroyCellData);
                    destroyCellData = new DestroyCellData();
                }

                for (int i = colIndex; i < Constants.COL_SIZE; i++)
                {
                    destroyCellData.Add(grid.cells2dArray[rowIndex, i]);
                }
                if (destroyCellData != null)
                {
                    destroyCellDatas.Add(destroyCellData);
                    destroyCellData = new DestroyCellData();
                }
            }
            if (grid.IsItColFull(colIndex))
            {
                for (int i = rowIndex; i >= 0; i--)
                {
                    destroyCellData.Add(grid.cells2dArray[i, colIndex]);
                }
                if (destroyCellData != null)
                {
                    destroyCellDatas.Add(destroyCellData);
                    destroyCellData = new DestroyCellData();
                }
                for (int i = rowIndex; i < Constants.RAW_SIZE; i++)
                {
                    destroyCellData.Add(grid.cells2dArray[i, colIndex]);
                }
                if (destroyCellData != null)
                {
                    destroyCellDatas.Add(destroyCellData);
                }
            }
            return destroyCellDatas;
        }
        #endregion

        #region ENTRY_RULE
        public void CheckEntryRule(SimplePolyomino polyomino)
        {
            int rowIndex = polyomino.dropCell.rowIndex, colIndex = polyomino.dropCell.colIndex;
            if (IsPlaceAvailable(polyomino))
            {
                SetTemporarySprite(polyomino, rowIndex, colIndex);
                SetTemporaryRowSprite(polyomino, rowIndex, colIndex);
                SetTemporaryColSprite(polyomino, rowIndex, colIndex);
            }
        }

        private void SetTemporarySprite(SimplePolyomino polyomino, int rowIndex, int colIndex)
        {
            Grid grid = GameManager.Instance.grid;
            foreach (PolyominoBlock polyominoBlocks in polyomino.polyominoBlocks)
            {
                grid.cells2dArray[rowIndex + polyominoBlocks.rowIndex, colIndex + polyominoBlocks.colIndex].SetTemporarySprite(polyomino.colorType, 0.5f);
            }
        }
        private void SetTemporaryRowSprite(SimplePolyomino polyomino, int rowIndex, int colIndex)
        {
            int length = Constants.RAW_SIZE;
            Grid grid = GameManager.Instance.grid;
            SpriteTypes colorType = polyomino.colorType;
            cells = new List<Cell>();
            for (int i = 0; i < polyomino.rowWiseDistributeDatas.Count; i++)
            {

                cells.Clear();

                for (int j = 0; j < length; j++)
                {
                    cells.Add(grid.cells2dArray[rowIndex + polyomino.rowWiseDistributeDatas[i].index, j]);
                }

                for (int j = 0; j < polyomino.rowWiseDistributeDatas[i].polyominoBlocks.Count; j++)
                {
                    cells.Remove(grid.cells2dArray[rowIndex + polyomino.rowWiseDistributeDatas[i].index,
                        colIndex + polyomino.rowWiseDistributeDatas[i].polyominoBlocks[j].colIndex]);
                }

                if (!IsAnyEmptyCell(cells))
                    for (int j = 0; j < cells.Count; j++)
                    {
                        cells[j].SetTemporarySprite(colorType, 1);
                    }
            }
        }
        private void SetTemporaryColSprite(SimplePolyomino polyomino, int rowIndex, int colIndex)
        {
            int length = Constants.COL_SIZE;
            Grid grid = GameManager.Instance.grid;
            SpriteTypes colorType = polyomino.colorType;
            cells = new List<Cell>();
            for (int i = 0; i < polyomino.colWiseDistributeDatas.Count; i++)
            {
                cells.Clear();

                for (int j = 0; j < length; j++)
                {
                    cells.Add(grid.cells2dArray[j, colIndex + polyomino.colWiseDistributeDatas[i].index]);
                }

                for (int j = 0; j < polyomino.colWiseDistributeDatas[i].polyominoBlocks.Count; j++)
                {
                    cells.Remove(grid.cells2dArray[rowIndex + polyomino.colWiseDistributeDatas[i].polyominoBlocks[j].rowIndex,
                        colIndex + polyomino.colWiseDistributeDatas[i].index]);
                }

                if (!IsAnyEmptyCell(cells))
                    for (int j = 0; j < cells.Count; j++)
                    {
                        cells[j].SetTemporarySprite(colorType, 1);
                    }
            }
        }

        private bool IsAnyEmptyCell(List<Cell> cells)
        {
            for (int j = 0; j < cells.Count; j++)
            {
                if (cells[j].IsEmpty())
                    return true;
            }
            return false;
        }
        #endregion

        #region EXIT_RULE
        public void CheckExitRule(Cell cell)
        {
            if (cell == null)
                return;
            //if (isChangedBlockSprite)
            {
                ResetTemporarySprite();
            }
        }
        public void ResetTemporarySprite()
        {
            Grid grid = GameManager.Instance.grid;
            grid.cells.ForEach(x => x.ResetTemporarySprite());
        }
        #endregion


        #region COMMON_RULE
        public bool IsPlaceAvailable(BasePolyomino polyomino)
        {
            return IsPlaceAvailableForSimplePolyomino((SimplePolyomino)polyomino);
        }

        private bool IsPlaceAvailableForSimplePolyomino(SimplePolyomino polyomino)
        {
            if (polyomino.dropCell == null)
                return false;
            int rowIndex = polyomino.dropCell.rowIndex, colIndex = polyomino.dropCell.colIndex;
            Grid grid = GameManager.Instance.grid;
            foreach (PolyominoBlock polyominoBlocks in polyomino.polyominoBlocks)
            {
                if (!IsItValidIndexFroRowAndCol(rowIndex + polyominoBlocks.rowIndex) || !IsItValidIndexFroRowAndCol(colIndex + polyominoBlocks.colIndex))
                {
                    return false;
                }
                if (!grid.cells2dArray[rowIndex + polyominoBlocks.rowIndex, colIndex + polyominoBlocks.colIndex].IsEmpty())
                {
                    return false;
                }
            }
            return true;
        }

        private bool IsItValidIndexFroRowAndCol(int index)
        {
            if (index < 0 || index >= Constants.RAW_SIZE)
                return false;
            return true;
        }


        #endregion

        #region BOMB_RULE

        public void CheckEntryRuleForBomb(List<Cell> dropCells)
        {
            Grid grid = GameManager.Instance.grid;
            int rowIndex;
            int colIndex;
            foreach (Cell cell in dropCells)
            {
                rowIndex = cell.rowIndex;
                colIndex = cell.colIndex;
                grid.SetCellEffectsByRow(rowIndex);
                grid.SetCellEffectsByCol(colIndex);
            }
        }


        #region EXIT_RULE
        public void CheckExitRuleForBomb()
        {
            Grid grid = GameManager.Instance.grid;
            grid.cells.ForEach(x => x.ChangeCellEffectLayer(-2));
        }
        #endregion

        #endregion
        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
    [System.Serializable]
    public class DestroyCellData
    {
        public List<Cell> cells = new List<Cell>();

        public void Add(Cell cell)
        {
            cells.Add(cell);
        }
    }
}