﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class SoundManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static SoundManager Instance;
        public bool Sound { get { return (PlayerPrefs.GetInt(Constants.SOUND, 1) == 1) ? true : false; } set { PlayerPrefs.SetInt(Constants.SOUND, value ? 1 : 0); } }
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                SetSound();
            }
        }
        #endregion

        #region PUBLIC_FUNCTIONS
        public void ToggleSound()
        {
            Sound = !Sound;
            SetSound();
        }
        public void SetSound()
        {
            AudioController.SetCategoryVolume("Sound", Sound ? 1f : 0.01f);
        }
        public void PlayBackgroundMusic()
        {
            AudioController.Play("BackGroundMusic");
        }
        public void PlayButtonClickSFX()
        {
            AudioController.Play("ButtonClick");
        }
        public void PlayGameStart()
        {
            AudioController.Play("GameStart");
        }
        public void PlayGameWinSFX()
        {
            //  AudioController.Play("GameWin");
        }
        public void PlayGameLooseSFX()
        {
            AudioController.Play("GameLoose");
        }
        public void PlayNotEnoughBoosterSFX()
        {
            // AudioController.Play("NotEnoughBooster");
        }
        public void PlayDestorySFX()
        {
            AudioController.Play("Destory");
        }
        public void PlayDropPolyomino()
        {
            AudioController.Play("Drop");
        }
        public void PlayBombSound()
        {
            AudioController.Play("Bomb");
        }
        public void PlayRainbowSound()
        {
            AudioController.Play("RainBow");
        }
        public void PlayCoolSound()
        {
            AudioController.Play("Cool");
        }
        public void PlayGoodSound()
        {
            AudioController.Play("Good");
        }
        public void PlayAmazingSound()
        {
            AudioController.Play("Amazing");
        }


        public void PlaySpinWheelSound()
        {
            AudioController.Play("SpinWheelBG");
        }

        public void StopSpinWheelSound()
        {
            AudioController.Stop("SpinWheelBG");
        }

        public void PlaySpinRewardSound()
        {
            AudioController.Play("SpinWheelReward");
        }

        public void PlaySpinRewardClime()
        {
            AudioController.Stop("SpinWheelReward");
            AudioController.Play("SpinWheelRewardClime");
        }
        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}