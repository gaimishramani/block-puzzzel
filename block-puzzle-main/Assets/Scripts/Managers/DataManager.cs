﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class DataManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static DataManager Instance;
        private int polyominoGeneratorCount;
        private bool isFirstContinueGame;

        public string LastTimeToSpin
        {
            get
            {
                if (!PlayerPrefs.HasKey(Constants.LAST_TIME_TO_SPIN))
                    PlayerPrefs.SetString(Constants.LAST_TIME_TO_SPIN, TimeHandler.SerializeDate(System.DateTime.Now));
                return PlayerPrefs.GetString(Constants.LAST_TIME_TO_SPIN, TimeHandler.SerializeDate(System.DateTime.Now));
            }
            set
            {
                PlayerPrefs.SetString(Constants.LAST_TIME_TO_SPIN, value);
            }
        }
        public bool IsFreeSpinAvailable
        {
            get
            {
                if (PlayerPrefs.HasKey(Constants.IS_SPIN_AVAILABLE))
                    return true;
                return false;
            }
        }

        public bool IsContinueGame
        {
            get
            {
                if(!isFirstContinueGame)
                {
                    isFirstContinueGame = true;
                    return PlayerPrefs.GetInt(Constants.CURRENT_SCORE,0) < PlayerPrefs.GetInt(Constants.HIGH_SCORE,0);
                }
                return false;
            }
        }
        //public bool IsItStaringPolyominoPhase
        //{
        //    get
        //    {
        //        if (!PlayerPrefs.HasKey(Constants.IS_IT_STARTING_POLYOMINO_PHASE))
        //            PlayerPrefs.SetInt(Constants.IS_IT_STARTING_POLYOMINO_PHASE, 0);
                
        //        if (PlayerPrefs.GetInt(Constants.IS_IT_STARTING_POLYOMINO_PHASE) < 2)
        //        {
        //            PlayerPrefs.SetInt(Constants.IS_IT_STARTING_POLYOMINO_PHASE, PlayerPrefs.GetInt(Constants.IS_IT_STARTING_POLYOMINO_PHASE) + 1);
        //            return true;
        //        }
        //        return false;
        //    }
        //}
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
            isFirstContinueGame = false;
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        public void ResetPolyominoGeneratorCount()
        {
            isFirstContinueGame = false;
            polyominoGeneratorCount = 0;
        }

        
        public void SetFreeSpinAvailable()
        {
            PlayerPrefs.SetInt(Constants.IS_SPIN_AVAILABLE, 0);
        }
        public void ResetFreeSpinAvailable()
        {
            PlayerPrefs.DeleteKey(Constants.IS_SPIN_AVAILABLE);
        }

        public bool IsItStaringPolyominoPhase()
        {

            polyominoGeneratorCount++;
            if (polyominoGeneratorCount<2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}