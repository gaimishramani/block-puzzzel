﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class UiManager : MonoBehaviour
    {
        #region PUBLIC_VARS
        public static UiManager Instance;
        public GameMainHomeView gameMainHomeView;
        public HalloweenHomeView halloweenHomeView;
        public ChristmasHomeView christmasHomeView;
        public EsterHomePlayView esterHomePlayView;
        public GamePlayView[] gamePlayViews;
        public TimerBasedGamePlay[] timerBasedGamePlayViews;
        public SettingView settingView;
        public GameOverView gameOverView;
        public GameLossView gameLossView;
        public GameQuitView gameQuitView;
        public RateUsView rateUsView;
        public FeedBackView feedBackView;
        public LeaderBoardView leaderBoardView;
        public PlayfabManager playfabManager;
        public ThemeSelectionView themeSelectionView;
        public GridUIManager gridUIManager;
        #endregion

        #region PRIVATE_VARS
        #endregion

        #region UNITY_CALLBACKS
        private void Awake()
        {
            Instance = this;
        }
        #endregion

        #region PUBLIC_FUNCTIONS

        public GamePlayView GetGamePlayViewByType(GamePlayType gamePlayType)
        {
            for (int i = 0; i < gamePlayViews.Length; i++)
            {
                if (gamePlayViews[i].gamePlayType == gamePlayType)
                {
                    return gamePlayViews[i];
                }
            }
            return null;
        }

        public TimerBasedGamePlay GetTimerBasedGamePlayViewByType(GamePlayType gamePlayType)
        {
            for (int i = 0; i < timerBasedGamePlayViews.Length; i++)
            {
                if (timerBasedGamePlayViews[i].gamePlayType == gamePlayType)
                {
                    return timerBasedGamePlayViews[i];
                }
            }
            return null;
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }
}