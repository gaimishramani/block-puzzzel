﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GamePlay
{
    public class DataAnalyster : MonoBehaviour
    {
        #region PUBLIC_VARS

        public static DataAnalyster Instance;

        public BreakingBlockGameData gameData;

        #endregion

        #region PRIVATE_VARS

        #endregion

        #region UNITY_CALLBACKS4

        private void Awake()
        {
            Instance = this;
            gameData = new BreakingBlockGameData();
        }

        #endregion

        #region PLAYER_PREFS_CALLS

        public void SetInt(string Key,int value)
        {
            PlayerPrefs.SetInt(Key, value);
        }

        public void SetFloat(string Key, float value)
        {
            PlayerPrefs.SetFloat(Key, value);
        }

        public void SetString(string Key, string value)
        {
            PlayerPrefs.SetString(Key, value);
        }

        public int GetInt(string Key,int defaultValue = 0)
        {
           return PlayerPrefs.GetInt(Key, defaultValue);
        }

        public float GetFloat(string Key, float defaultValue = 0)
        {
           return PlayerPrefs.GetFloat(Key, defaultValue);
        }

        public string GetString(string Key, string defaultValue = "")
        {
            return PlayerPrefs.GetString(Key, defaultValue);
        }



        #endregion

        #region PUBLIC_FUNCTIONS

        public void SetDataOnGameComplite(int score)
        {
            int totalGamePlayed = GetInt(Constants.TOTAL_NO_OF_GAME_PLAYED);
            double avgScore;
            avgScore = GetFloat(Constants.AVERAGE_SCORE);
            avgScore = (avgScore * totalGamePlayed)+score;
            totalGamePlayed++;
            avgScore /= totalGamePlayed;
            SetInt(Constants.TOTAL_NO_OF_GAME_PLAYED, totalGamePlayed);
            SetFloat(Constants.AVERAGE_SCORE, (float)avgScore);
        }

        public void IncrementSpinCount()
        {
            int spinCount = GetInt(Constants.SPIN_USE_COUNT);
            spinCount++;
            SetInt(Constants.SPIN_USE_COUNT,spinCount);
        }

        public void IncrementContinueGameCount()
        {
            int continueGameCount = GetInt(Constants.CONTINUE_GAME_COUNT);
            continueGameCount++;
            SetInt(Constants.CONTINUE_GAME_COUNT, continueGameCount);
        }

        public void IncrementOneBlockBoosterUseCount()
        {
            int oneBlockBoosterUseCount = GetInt(Constants.ONE_BLOCK_BOOSTER_USE_COUNT);
            oneBlockBoosterUseCount++;
            SetInt(Constants.ONE_BLOCK_BOOSTER_USE_COUNT, oneBlockBoosterUseCount);
        }

        public void IncrementRainbowBlockBoosterUseCount()
        {
            int temp = GetInt(Constants.RAINBOW_BLOCK_BOOSTER_USE_COUNT);
            temp++;
            SetInt(Constants.RAINBOW_BLOCK_BOOSTER_USE_COUNT, temp);
        }

        public void IncrementBigBombBoosterBoosterUseCount()
        {
            int temp = GetInt(Constants.BIG_BOMB_BLOCK_BOOSTER_USE_COUNT);
            temp++;
            SetInt(Constants.BIG_BOMB_BLOCK_BOOSTER_USE_COUNT, temp);
        }

        public void SaveGameData()
        {
            gameData.gameDataVersion = GetFloat(Constants.GAME_DATA_VERSION) + 0.001f;
            SetFloat(Constants.GAME_DATA_VERSION, gameData.gameDataVersion);
            gameData.bigBommUseCount = GetInt(Constants.BIG_BOMB_BLOCK_BOOSTER_USE_COUNT);
            gameData.rainbowBlockUseCount = GetInt(Constants.RAINBOW_BLOCK_BOOSTER_USE_COUNT);
            gameData.oneBlockBoosterUseCount = GetInt(Constants.ONE_BLOCK_BOOSTER_USE_COUNT);
            gameData.totalNoGamePlayed = GetInt(Constants.TOTAL_NO_OF_GAME_PLAYED);
            gameData.totalSpinUseCount = GetInt(Constants.SPIN_USE_COUNT);
            gameData.totalContinueGameUseCount = GetInt(Constants.CONTINUE_GAME_COUNT);
            gameData.averageScore = GetFloat(Constants.AVERAGE_SCORE);
        }

        public string GetGameData()
        {
            SaveGameData();
            string data = JsonUtility.ToJson(gameData);
            return data;
        }

        public void LoadData(string gameDataString)
        {
            this.gameData = JsonUtility.FromJson<BreakingBlockGameData>(gameDataString);
            SetFloat(Constants.GAME_DATA_VERSION, gameData.gameDataVersion);
            SetInt(Constants.BIG_BOMB_BLOCK_BOOSTER_USE_COUNT, gameData.bigBommUseCount);
            SetInt(Constants.RAINBOW_BLOCK_BOOSTER_USE_COUNT, gameData.rainbowBlockUseCount);
            SetInt(Constants.ONE_BLOCK_BOOSTER_USE_COUNT, gameData.oneBlockBoosterUseCount);
            SetInt(Constants.TOTAL_NO_OF_GAME_PLAYED, gameData.totalNoGamePlayed);
            SetInt(Constants.SPIN_USE_COUNT, gameData.totalSpinUseCount);
            SetInt(Constants.CONTINUE_GAME_COUNT, gameData.totalContinueGameUseCount);
            SetFloat(Constants.AVERAGE_SCORE, gameData.averageScore);
        }

        #endregion

        #region PRIVATE_FUNCTIONS
        #endregion

        #region CO-ROUTINES
        #endregion

        #region EVENT_HANDLERS
        #endregion

        #region UI_CALLBACKS
        #endregion
    }

    public class BreakingBlockGameData
    {
        public float gameDataVersion;
        public int bigBommUseCount;
        public int rainbowBlockUseCount;
        public int oneBlockBoosterUseCount;
        public int totalNoGamePlayed;
        public int totalSpinUseCount;
        public int totalContinueGameUseCount;
        public float averageScore;
    }

}